﻿/***
 * Library - Tina
 *
 * Useful methods for time and date
 * 
 ***/
(function(global) {
	'use strict';

	/**
	 * Formate a date for display.
	 * - Simulates PHP's date() function
	 * @param: {string} format
	 *         {object | number} Date object, or a UNIX timestamp (in ms)
	 * @return {string} formated string
	 */
	function formatDate(format, date) {
		// Check Params
		if (typeof format !== 'string') {
			throw new TypeError('formatDate() - invalid format string.');
		}
		if (!(date instanceof Date)) {
			date = new Date(parseInt(date));
		}
		if (isNaN(date.getTime())) {
			throw new TypeError('formatDate() - invalid date.');
		}
		
		// Constants
		var MONTHS_SHORT = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		var MONTHS_LONG = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var DAYS_SHORT = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
		var DAYS_LONG = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		
		var returnStr = '';
		var replace = {
			// Day
			d: function() { return (date.getDate() < 10 ? '0' : '') + date.getDate(); },
			D: function() { return DAYS_SHORT[date.getDay()]; },
			j: function() { return date.getDate(); },
			l: function() { return DAYS_LONG[date.getDay()]; },
			N: function() { return date.getDay() + 1; },
			S: function() { return (date.getDate() % 10 === 1 && date.getDate() !== 11 ? 'st' : (date.getDate() % 10 === 2 && date.getDate() !== 12 ? 'nd' : (date.getDate() % 10 === 3 && date.getDate() !== 13 ? 'rd' : 'th'))); },
			w: function() { return date.getDay(); },
			z: function() { var d = new Date(date.getFullYear(),0,1); return Math.ceil((this - d) / 86400000); },
			// Week
			W: function() { var d = new Date(date.getFullYear(), 0, 1); return Math.ceil((((this - d) / 86400000) + d.getDay() + 1) / 7); },
			// Month
			F: function() { return MONTHS_LONG[date.getMonth()]; },
			m: function() { return (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1); },
			M: function() { return MONTHS_SHORT[date.getMonth()]; },
			n: function() { return date.getMonth() + 1; },
			t: function() { var d = new Date(); return new Date(d.getFullYear(), d.getMonth(), 0).getDate(); },
			// Year
			L: function() { var year = date.getFullYear(); return (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)); },
			o: function() { var d = new Date(date.valueOf()); d.setDate(d.getDate() - ((date.getDay() + 6) % 7) + 3); return d.getFullYear();},
			Y: function() { return date.getFullYear(); },
			y: function() { return ('' + date.getFullYear()).substr(2); },
			// Time
			a: function() { return date.getHours() < 12 ? 'am' : 'pm'; },
			A: function() { return date.getHours() < 12 ? 'AM' : 'PM'; },
			B: function() { return Math.floor((((date.getUTCHours() + 1) % 24) + date.getUTCMinutes() / 60 + date.getUTCSeconds() / 3600) * 1000 / 24); },
			g: function() { return date.getHours() % 12 || 12; },
			G: function() { return date.getHours(); },
			h: function() { return ((date.getHours() % 12 || 12) < 10 ? '0' : '') + (date.getHours() % 12 || 12); },
			H: function() { return (date.getHours() < 10 ? '0' : '') + date.getHours(); },
			i: function() { return (date.getMinutes() < 10 ? '0' : '') + date.getMinutes(); },
			s: function() { return (date.getSeconds() < 10 ? '0' : '') + date.getSeconds(); },
			u: function() { var m = date.getMilliseconds(); return (m < 10 ? '00' : (m < 100 ?'0' : '')) + m; },
			// Timezone
			e: function() { return 'Not Yet Supported'; },
			I: function() {
				var DST = null;
				for (var i = 0; i < 12; ++i) {
					var d = new Date(date.getFullYear(), i, 1);
					var offset = d.getTimezoneOffset();
		
					if (DST === null) {
						DST = offset;
					}
					else if (offset < DST) {
						DST = offset;
						break;
					}
					else if (offset > DST) {
						break;
					}
				}
				return (date.getTimezoneOffset() === DST) || 0;
			},
			O: function() { return (-date.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(date.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(date.getTimezoneOffset() / 60)) + '00'; },
			P: function() { return (-date.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(date.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(date.getTimezoneOffset() / 60)) + ':00'; },
			T: function() { var m = date.getMonth(); date.setMonth(0); var result = date.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1'); date.setMonth(m); return result;},
			Z: function() { return -date.getTimezoneOffset() * 60; },
			// Full Date/Time
			c: function() { return formatDate('Y-m-d\\TH:i:sP', date); },
			r: function() { return date.toString(); },
			U: function() { return date.getTime() / 1000; }
		};
		
		for (var i = 0; i < format.length; i++) {
			var curChar = format.charAt(i);
			if (i - 1 >= 0 && format.charAt(i - 1) === '\\') {
				returnStr += curChar;
			}
			else if (replace[curChar]) {
				//returnStr += replace[curChar].call(this);
				returnStr += replace[curChar]();
			}
			else if (curChar !== '\\'){
				returnStr += curChar;
			}
		}
		return returnStr;
	} //formatDate()
	
	/**
	 * Checks number of days in a given month.
	 * - Note: month is 0-based, i.e. 0 refers to January
	 * @param: {Date} or {number, number} - yy, mm.
	 * @return: {number}
	 */
	function getDaysInMonth() {
		var m, y;
		if (arguments.length === 1) {
			if (arguments[0] instanceof Date) {
				m = arguments[0].getMonth();
				y = arguments[0].getFullYear();
			}
		}
		else if (arguments.length === 2) {
			m = parseInt(arguments[1]);
			y = parseInt(arguments[0]);
		}
		if (isNaN(m) || isNaN(y)) {
			throw new TypeError('getDaysInMonth() - invalid param(s).');
		}
		if (m < 0 || m > 11) {
			throw new RangeError('getDaysInMonth() - m must be [0-11].');
		}
		
		return [31, (isLeapYear(y)? 29 : 28) , 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m];
	} //getDaysInMonth()
	
	/** 
	 * Checks is a year is leap year
	 * @param: {Date | number} y - will be parsed into integer representing year
	 * @return: {bool}
	 */
	function isLeapYear(x) {
		if (x instanceof Date) {
			x = x.getFullYear();
		}

		x = parseInt(x);
		return ((x%4 === 0 && x%100 !== 0) || x%400 === 0 );
	} //isLeapYear()
	
	/**
	 * Checks if a given yyyy, mm, dd is a valid date in calendar
	 * - Note: month is 0-based, i.e. 0 refers to January
	 * @param: {number, number, number} y, m, d
	 * @return {bool}
	 */
	function isDate(y, m, d) {
		y = parseInt(y);
		m = parseInt(m);
		d = parseInt(d);

		if (isNaN(y) || isNaN(m) || isNaN(d)) {
			return false;
		}
		if (m < 0 || m > 11) {
			return false;
		}

		var max_d = getDaysInMonth(y, m);
		if (d < 1 || d > max_d) {
			return false;
		}

		return true;
	} //isDate()
	
	/**
	 * Checks if a given hh:mm:ss:sss is a valid time (24H format)
	 * @param: {number, number, [number], [number]} - ss & ms optional.
	 * @return: {bool}
	 */
	function isTime(hh,mm,ss,ms) {
		if (typeof ss === 'undefined') {
			ss = 0;
		}
		if (typeof ms === 'undefined') {
			ms = 0;
		}

		hh = parseInt(hh);
		mm = parseInt(mm);
		ss = parseInt(ss);

		if (isNaN(hh) || isNaN(mm) || isNaN(ss)) {
			return false;
		}

		if (hh < 0 || hh > 23) {
			return false;
		}
		if (mm < 0 || mm > 59) {
			return false;
		}
		if (ss < 0 || ss > 59) {
			return false;
		}
		if (ms < 0 || ms > 999) {
			return false;
		}
		return true;
	} //isTime()

	/**
	 * Gets the zodiac for a certain date
	 * - Note: month is 0-based, i.e. 0 refers to January
	 * - Note: if year is not provided, year 2000 is assumed.
	 * @param: {Date}
	 *      or {number, number} month, day.
	 *      or {number, number, number} year, month, day
	 * @return: {string}
	 */
	function getZodiac() {
		var y, m, d;

		// Parse and check args
		if (arguments.length === 3) {
			y = arguments[0];
			m = arguments[1];
			d = arguments[2];
			if (typeof y !== 'number') {
				throw TypeError('getZodiac() - Expect year (1st of 3 args) to be number. Got: ' + (typeof y));
			}
			if (typeof m !== 'number') {
				throw TypeError('getZodiac() - Expect month (2nd of 3 args) to be number. Got: ' + (typeof m));
			}
			if (typeof d !== 'number') {
				throw TypeError('getZodiac() - Expect day (3rd of 3 args) to be number. Got: ' + (typeof d));
			}
		}
		else if (arguments.length === 2) {
			y = 2000;
			m = arguments[0];
			d = arguments[1];
			if (typeof m !== 'number') {
				throw TypeError('getZodiac() - Expect month (1st of 2 args) to be number. Got: ' + (typeof m));
			}
			if (typeof d !== 'number') {
				throw TypeError('getZodiac() - Expect day (2nd of 2 args) to be number. Got: ' + (typeof d));
			}
		}
		else if (arguments.length === 1) {
			if (arguments[0] instanceof Date) {
				y = arguments[0].getFullYear();
				m = arguments[0].getMonth() ;
				d = arguments[0].getDate();
			}
			else {
				throw TypeError('getZodiac() - Expect only argument to be Date object.');
			}
		}
		else {
			throw TypeError('getZodiac() - Expect 1 to 3 arguments. Found: ' + arguments.length);
		}

		// Check valid date
		if (!isDate(y, m, d)) {
			throw TypeError('getZodiac() - Invalid date');
		}

		m += 1;
		if (m === 1 && d >=20 || m === 2 && d <=18) return 'Aquarius';
		if (m === 2 && d >=19 || m === 3 && d <=20) return 'Pisces';
		if (m === 3 && d >=21 || m === 4 && d <=19) return 'Aries';
		if (m === 4 && d >=20 || m === 5 && d <=20) return 'Taurus';
		if (m === 5 && d >=21 || m === 6 && d <=21) return 'Gemini';
		if (m === 6 && d >=22 || m === 7 && d <=22) return 'Cancer';
		if (m === 7 && d >=23 || m === 8 && d <=22) return 'Leo';
		if (m === 8 && d >=23 || m === 9 && d <=22) return 'Virgo';
		if (m === 9 && d >=23 || m === 10 && d <=22) return 'Libra';
		if (m === 10 && d >=23 || m === 11 && d <=21) return 'Scorpio';
		if (m === 11 && d >=22 || m === 12 && d <=21) return 'Sagittarius';
		if (m === 12 && d >=22 || m === 1 && d <=19) return 'Capricorn';
	} //getZodiac()

	/**
	 * Gets the Chinese zodiac for a certain date
	 * - Note: month is 0-based, i.e. 0 refers to January
	 * @param: {Date}
	 *      or {number, number, number} year, month, day
	 * @return: {string}
	 */
	function getChineseZodiac() {
		var y, m, d;

		// Parse and check args
		if (arguments.length === 3) {
			y = arguments[0];
			m = arguments[1];
			d = arguments[2];
			if (typeof y !== 'number') {
				throw TypeError('getChineseZodiac() - Expect year (1st of 3 args) to be number. Got: ' + (typeof y));
			}
			if (typeof m !== 'number') {
				throw TypeError('getChineseZodiac() - Expect month (2nd of 3 args) to be number. Got: ' + (typeof m));
			}
			if (typeof d !== 'number') {
				throw TypeError('getChineseZodiac() - Expect day (3rd of 3 args) to be number. Got: ' + (typeof d));
			}
		}
		else if (arguments.length === 1) {
			if (arguments[0] instanceof Date) {
				y = arguments[0].getFullYear();
				m = arguments[0].getMonth() ;
				d = arguments[0].getDate();
			}
			else {
				throw TypeError('getChineseZodiac() - Expect only argument to be Date object.');
			}
		}
		else {
			throw TypeError('getChineseZodiac() - Expect 1 OR 3 arguments. Found: ' + arguments.length);
		}

		// Check valid date
		if (!isDate(y, m, d)) {
			throw TypeError('getChineseZodiac() - Invalid date');
		}

		m += 1;

		// Chinese zodiac LUT
		var animals = ['Monkey', 'Rooster', 'Dog', 'Pig', 'Rat', 'Ox', 'Tiger', 'Rabbit', 'Dragon', 'Snake', 'Horse', 'Ram'];
		var start_dates = {
			'Rooster': {
				'1897': [2, 2],
				'1909': [1, 22],
				'1921': [2, 8],
				'1933': [1, 25],
				'1945': [2, 13],
				'1957': [1, 30],
				'1969': [2, 17],
				'1981': [2, 5],
				'1993': [1, 23],
				'2005': [2, 9],
				'2017': [1, 28],
				'2029': [2, 13],
				'2041': [2, 1]
			},
			'Dog': {
				'1898': [1, 22],
				'1910': [2, 10],
				'1922': [1, 28],
				'1934': [2, 14],
				'1946': [2, 2],
				'1958': [2, 18],
				'1970': [2, 6],
				'1982': [1, 25],
				'1994': [2, 10],
				'2006': [1, 29],
				'2018': [2, 16],
				'2030': [2, 3],
				'2042': [1, 22]
			},
			'Pig': {
				'1899': [2, 10],
				'1911': [1, 30],
				'1923': [2, 16],
				'1935': [2, 3],
				'1947': [1, 22],
				'1959': [2, 8],
				'1971': [1, 27],
				'1983': [2, 13],
				'1995': [1, 31],
				'2007': [2, 18],
				'2019': [2, 5],
				'2031': [1, 23],
				'2043': [2, 10]
			},
			'Rat': {
				'1900': [1, 31],
				'1912': [2, 18], 
				'1924': [2, 5], 
				'1936': [1, 24], 
				'1948': [2, 10], 
				'1960': [1, 28], 
				'1972': [2, 15], 
				'1984': [2, 2], 
				'1996': [2, 19], 
				'2008': [2, 7], 
				'2020': [1, 25], 
				'2032': [2, 11]
			},
			'Ox': {
				'1901': [2, 19],
				'1913': [2, 6],
				'1925': [1, 24],
				'1937': [2, 11],
				'1949': [1, 29],
				'1961': [2, 15],
				'1973': [2, 3],
				'1985': [2, 20],
				'1997': [2, 7],
				'2009': [1, 26],
				'2021': [2, 12],
				'2033': [1, 31]
			},
			'Tiger': {
				'1902': [2, 8],
				'1914': [1, 26],
				'1926': [2, 13],
				'1938': [1, 31],
				'1950': [2, 17],
				'1962': [2, 5],
				'1974': [1, 23],
				'1986': [2, 9],
				'1998': [1, 28],
				'2010': [2, 14],
				'2022': [2, 1],
				'2034': [2, 19]
			},
			'Rabbit': {
				'1903': [1, 29],
				'1915': [2, 14],
				'1927': [2, 2],
				'1939': [2, 19],
				'1951': [2, 6],
				'1963': [1, 25],
				'1975': [2, 11],
				'1987': [1, 29],
				'1999': [2, 16],
				'2011': [2, 3],
				'2023': [1, 22],
				'2035': [2, 8]
			},
			'Dragon': {
				'1904': [2, 16],
				'1916': [2, 3],
				'1928': [1, 22],
				'1940': [2, 8],
				'1952': [1, 27],
				'1964': [2, 13],
				'1976': [1, 31],
				'1988': [2, 17],
				'2000': [2, 5],
				'2012': [1, 23],
				'2024': [2, 10],
				'2036': [1, 28]
			},
			'Snake': {
				'1905': [2, 4],
				'1917': [1, 23],
				'1929': [2, 9],
				'1941': [1, 27],
				'1953': [2, 14],
				'1965': [2, 2],
				'1977': [2, 18],
				'1989': [2, 6],
				'2001': [1, 24],
				'2013': [2, 10],
				'2025': [1, 29],
				'2037': [2, 15]
			},
			'Horse': {
				'1906': [1, 25],
				'1918': [2, 11],
				'1930': [1, 29],
				'1942': [2, 15],
				'1954': [2, 3],
				'1966': [1, 21],
				'1978': [2, 7],
				'1990': [1, 27],
				'2002': [2, 12],
				'2014': [1, 31],
				'2026': [1, 17],
				'2038': [2, 4]
			},
			'Ram': {
				'1907': [2, 13],
				'1919': [2, 1],
				'1931': [2, 17],
				'1943': [2, 5],
				'1955': [1, 24],
				'1967': [2, 9],
				'1979': [1, 28],
				'1991': [2, 14],
				'2003': [2, 1],
				'2015': [2, 19],
				'2027': [2, 6],
				'2039': [1, 24]
			},
			'Monkey': {
				'1908': [2, 2],
				'1920': [2, 20],
				'1932': [2, 6],
				'1944': [1, 25],
				'1956': [2, 12],
				'1968': [1, 30],
				'1980': [2, 16],
				'1992': [2, 4],
				'2004': [1, 22],
				'2016': [2, 8],
				'2028': [1, 26],
				'2040': [2, 12]
			}
		};
		
		// Make a good guess based on year
		var guess_idx = y % 12;
		var guess = animals[guess_idx];
		var tmp = start_dates[guess][y];

		// Check if our guess is right
		if (m > tmp[0]) {
			return guess;
		}
		if (m === tmp[0] && d >= tmp[1]) {
			return guess;
		}

		// Guess is wrong
		guess_idx--;
		if (guess_idx === -1) {
			guess_idx = 11;
		}
		return animals[guess_idx];
	} //getChineseZodiac()
	
	global.tina = {
		formatDate: formatDate,
		getDaysInMonth: getDaysInMonth,
		isLeapYear: isLeapYear,
		isDate: isDate,
		isTime: isTime,
		getZodiac: getZodiac,
		getChineseZodiac: getChineseZodiac
	};
})(this);
