/***
 * Library - Tina
 *
 * Useful methods for time and date
 * 
 ***/
(function(global) {
	'use strict';

	/**
	 * Formate a date for display.
	 * - Simulates PHP's date() function
	 * @param: {string} format
	 *         {object | number} Date object, or a UNIX timestamp (in ms)
	 * @return {string} formated string
	 */
	function formatDate(format, date) {
		// Check Params
		if (typeof format !== 'string') {
			throw new TypeError('formatDate() - invalid format string.');
		}
		if (!(date instanceof Date)) {
			date = new Date(parseInt(date));
		}
		if (isNaN(date.getTime())) {
			throw new TypeError('formatDate() - invalid date.');
		}
		
		// Constants
		var MONTHS_SHORT = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		var MONTHS_LONG = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var DAYS_SHORT = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
		var DAYS_LONG = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		
		var returnStr = '';
		var replace = {
			// Day
			d: function() { return (date.getDate() < 10 ? '0' : '') + date.getDate(); },
			D: function() { return DAYS_SHORT[date.getDay()]; },
			j: function() { return date.getDate(); },
			l: function() { return DAYS_LONG[date.getDay()]; },
			N: function() { return date.getDay() + 1; },
			S: function() { return (date.getDate() % 10 === 1 && date.getDate() !== 11 ? 'st' : (date.getDate() % 10 === 2 && date.getDate() !== 12 ? 'nd' : (date.getDate() % 10 === 3 && date.getDate() !== 13 ? 'rd' : 'th'))); },
			w: function() { return date.getDay(); },
			z: function() { var d = new Date(date.getFullYear(),0,1); return Math.ceil((this - d) / 86400000); },
			// Week
			W: function() { var d = new Date(date.getFullYear(), 0, 1); return Math.ceil((((this - d) / 86400000) + d.getDay() + 1) / 7); },
			// Month
			F: function() { return MONTHS_LONG[date.getMonth()]; },
			m: function() { return (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1); },
			M: function() { return MONTHS_SHORT[date.getMonth()]; },
			n: function() { return date.getMonth() + 1; },
			t: function() { var d = new Date(); return new Date(d.getFullYear(), d.getMonth(), 0).getDate(); },
			// Year
			L: function() { var year = date.getFullYear(); return (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)); },
			o: function() { var d = new Date(date.valueOf()); d.setDate(d.getDate() - ((date.getDay() + 6) % 7) + 3); return d.getFullYear();},
			Y: function() { return date.getFullYear(); },
			y: function() { return ('' + date.getFullYear()).substr(2); },
			// Time
			a: function() { return date.getHours() < 12 ? 'am' : 'pm'; },
			A: function() { return date.getHours() < 12 ? 'AM' : 'PM'; },
			B: function() { return Math.floor((((date.getUTCHours() + 1) % 24) + date.getUTCMinutes() / 60 + date.getUTCSeconds() / 3600) * 1000 / 24); },
			g: function() { return date.getHours() % 12 || 12; },
			G: function() { return date.getHours(); },
			h: function() { return ((date.getHours() % 12 || 12) < 10 ? '0' : '') + (date.getHours() % 12 || 12); },
			H: function() { return (date.getHours() < 10 ? '0' : '') + date.getHours(); },
			i: function() { return (date.getMinutes() < 10 ? '0' : '') + date.getMinutes(); },
			s: function() { return (date.getSeconds() < 10 ? '0' : '') + date.getSeconds(); },
			u: function() { var m = date.getMilliseconds(); return (m < 10 ? '00' : (m < 100 ?'0' : '')) + m; },
			// Timezone
			e: function() { return 'Not Yet Supported'; },
			I: function() {
				var DST = null;
				for (var i = 0; i < 12; ++i) {
					var d = new Date(date.getFullYear(), i, 1);
					var offset = d.getTimezoneOffset();
		
					if (DST === null) {
						DST = offset;
					}
					else if (offset < DST) {
						DST = offset;
						break;
					}
					else if (offset > DST) {
						break;
					}
				}
				return (date.getTimezoneOffset() === DST) || 0;
			},
			O: function() { return (-date.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(date.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(date.getTimezoneOffset() / 60)) + '00'; },
			P: function() { return (-date.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(date.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(date.getTimezoneOffset() / 60)) + ':00'; },
			T: function() { var m = date.getMonth(); date.setMonth(0); var result = date.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1'); date.setMonth(m); return result;},
			Z: function() { return -date.getTimezoneOffset() * 60; },
			// Full Date/Time
			c: function() { return formatDate('Y-m-d\\TH:i:sP', date); },
			r: function() { return date.toString(); },
			U: function() { return date.getTime() / 1000; }
		};
		
		for (var i = 0; i < format.length; i++) {
			var curChar = format.charAt(i);
			if (i - 1 >= 0 && format.charAt(i - 1) === '\\') {
				returnStr += curChar;
			}
			else if (replace[curChar]) {
				//returnStr += replace[curChar].call(this);
				returnStr += replace[curChar]();
			}
			else if (curChar !== '\\'){
				returnStr += curChar;
			}
		}
		return returnStr;
	} //formatDate()
	
	/**
	 * Checks number of days in a given month.
	 * - Note: month is 0-based, i.e. 0 refers to January
	 * @param: {Date} or {number, number} - yy, mm.
	 * @return: {number}
	 */
	function getDaysInMonth() {
		var m, y;
		if (arguments.length === 1) {
			if (arguments[0] instanceof Date) {
				m = arguments[0].getMonth();
				y = arguments[0].getFullYear();
			}
		}
		else if (arguments.length === 2) {
			m = parseInt(arguments[1]);
			y = parseInt(arguments[0]);
		}
		if (isNaN(m) || isNaN(y)) {
			throw new TypeError('getDaysInMonth() - invalid param(s).');
		}
		if (m < 0 || m > 11) {
			throw new RangeError('getDaysInMonth() - m must be [0-11].');
		}
		
		return [31, (isLeapYear(y)? 29 : 28) , 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m];
	} //getDaysInMonth()
	
	/** 
	 * Checks is a year is leap year
	 * @param: {Date | number} y - will be parsed into integer representing year
	 * @return: {bool}
	 */
	function isLeapYear(x) {
		if (x instanceof Date) {
			x = x.getFullYear();
		}

		x = parseInt(x);
		return ((x%4 === 0 && x%100 !== 0) || x%400 === 0 );
	} //isLeapYear()
	
	/**
	 * Checks if a given yyyy, mm, dd is a valid date in calendar
	 * - Note: month is 0-based, i.e. 0 refers to January
	 * @param: {number, number, number} y, m, d
	 * @return {bool}
	 */
	function isDate(y, m, d) {
		y = parseInt(y);
		m = parseInt(m);
		d = parseInt(d);

		if (isNaN(y) || isNaN(m) || isNaN(d)) {
			return false;
		}
		if (m < 0 || m > 11) {
			return false;
		}

		var max_d = getDaysInMonth(y, m);
		if (d < 1 || d > max_d) {
			return false;
		}

		return true;
	} //isDate()
	
	/**
	 * Checks if a given hh:mm:ss:sss is a valid time (24H format)
	 * @param: {number, number, [number], [number]} - ss & ms optional.
	 * @return: {bool}
	 */
	function isTime(hh,mm,ss,ms) {
		if (typeof ss === 'undefined') {
			ss = 0;
		}
		if (typeof ms === 'undefined') {
			ms = 0;
		}

		hh = parseInt(hh);
		mm = parseInt(mm);
		ss = parseInt(ss);

		if (isNaN(hh) || isNaN(mm) || isNaN(ss)) {
			return false;
		}

		if (hh < 0 || hh > 23) {
			return false;
		}
		if (mm < 0 || mm > 59) {
			return false;
		}
		if (ss < 0 || ss > 59) {
			return false;
		}
		if (ms < 0 || ms > 999) {
			return false;
		}
		return true;
	} //isTime()
	
	global.tina = {
		formatDate: formatDate,
		getDaysInMonth: getDaysInMonth,
		isLeapYear: isLeapYear,
		isDate: isDate,
		isTime: isTime
	};
})(this);
