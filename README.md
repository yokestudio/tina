# Tina #

A JavaScript library of functions meant for dates and times.

### Browser Support ###
* IE 9+
* Chrome
* Edge
* Firefox
* Opera
* Safari

### Set Up ###

1. Download the latest release (as of 12 Apr 2019, v1.3.1).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="tina-1.3.1.min.js"></script>`

### API ###
**`formatDate`**

```
/**
 * Formate a date for display.
 * - Simulates PHP's date() function
 * @param: {string} format
 *         {object | number} Date object, or a UNIX timestamp (in ms)
 * @return {string} formated string
 */

// Sample usage
var now = new Date();
console.log('Your system time is currently: ' + tina.formatDate('Y-m-d H:i:s', now));
```

**`getDaysInMonth`**

```
/**
 * Checks number of days in a given month.
 * - Note: month is 0-based, i.e. 0 refers to January
 * @param: {Date} or {number, number} - yy, mm.
 * @return: {number}
 */
 
// Sample usage
var now = new Date();
console.log('There are ' + tina.getDaysInMonth(now) + ' days in this month.');
console.log('There are ' + tina.getDaysInMonth(1999, 1) + ' days in Feb 1999.');
console.log('There are ' + tina.getDaysInMonth(2000, 1) + ' days in Feb 2000.');
```

**`isLeapYear`**

```
/** 
 * Checks is a year is leap year
 * @param: {Date | number} y - will be parsed into integer representing year
 * @return: {bool}
 */

// Sample usage
var now = new Date();
console.log(tina.isLeapYear(now)? 'This is a leap year.' : 'This is NOT a leap year');
console.log(tina.isLeapYear(1999)? '1999 is a leap year.' : '1999 is NOT a leap year');
console.log(tina.isLeapYear(2000)? '2000 is a leap year.' : '2000 is NOT a leap year');
```

**`isDate`**

```
/**
 * Checks if a given yyyy, mm, dd is a valid date in calendar
 * - Note: month is 0-based, i.e. 0 refers to January
 * @param: {number, number, number} y, m, d
 * @return {bool}
 */

// Sample usage
var yyyy = '2015'; // e.g. form data of birthday
var mm = '11';     // careful: JS uses 0-based month.
var dd = '20';
console.log('This is ' + (tina.isDate(yyyy, mm, dd)?'':'NOT ') + 'a valid date.');
```

**`isTime`**

```
/**
 * Checks if a given hh:mm:ss:sss is a valid time (24H format)
 * @param: {number, number, [number], [number]} - ss & ms optional.
 * @return: {bool}
 */

// Sample usage
var hh = 23;
var mm = 59;
var ss = 59;
var ms = 789;
console.log('This is ' + (tina.isTime(hh, mm, ss, ms)?'':'NOT ') + 'a valid time.');
```

**`getZodiac`**

```
/**
 * Gets the zodiac for a certain date
 * - Note: month is 0-based, i.e. 0 refers to January
 * - Note: if year is not provided, year 2000 is assumed.
 * @param: {Date}
 *      or {number, number} month, day.
 *      or {number, number, number} year, month, day
 * @return: {string}
 */

// Sample usage
var sign = tina.getZodiac(1983, 2, 21); // 'Aries' (for 21 Mar 1983)
```

**`getChineseZodiac`**

```
/**
 * Gets the Chinese zodiac for a certain date
 * - Note: month is 0-based, i.e. 0 refers to January
 * @param: {Date}
 *      or {number, number, number} year, month, day
 * @return: 
 */

// Sample usage
var animal = tina.getChineseZodiac(1983, 2, 21); // 'Pig' (for 21 Mar 1983)
```

### FAQ ###

1. Why is there no method to convert from UNIX timestamp to a Date object?
> This can be easily achieved using JavaScript's built-in Date constructor: `new Date(unix_timestamp*1000)`.

1. Why don't you use Date.js or Moment.js?
> More often than not, server-side APIs return strings or integer timestamps to the frontend. User input from HTML forms are also usually strings. Most use cases revolve around converting timestamps to human-readable strings, or vice versa. Tina's methods accept plain numbers and strings, meaning you can get work done quickly without the need for `Date` objects, which is really an extraneous intermediate data type, from the perspective of your application consumers (both your server-side application and the human users).
>
> Both Date.js and Moment.js work mainly with `Date` or `Moment` objects, an intermediate object type which we are trying to avoid. Don't get us wrong - these libraries are useful tools, but they serve a different purpose, e.g. date/time arithmetic. These are not exactly necessary for 99% of all projects. Tina, being a much smaller library focused on simpler stuff, is likely to be what most people need.

### Contact ###

* Email us at <yokestudio@hotmail.com>.